<?php
include_once 'functions.php';
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 style="text-align: center;padding: 20px 0;">Магазин Телефонов</h1>
                <p style="padding: 20px 0"></p>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cart">
                    Корзина <?php echo count($_SESSION["items"])?>
                </button>
            </div>

            <?php foreach($data as $item):?>
            <div class="col-md-4 col-sm-12 item" >
                <h2><?php echo $item['title']?></h2>
                <img style="width: 200px;" src="<?php echo $item['img']?>" alt="">
                <p>Цена <?php echo $item['price']?> $</p>
                <form method="post">
                    <input type="hidden" name="name" value="<?=$item['title']?>">
                    <input type="hidden" name="price" value="<?=$item['price']?>">
                    <select name="quantity">
                        <option value="1">Выбрать 1 штуку</option>
                        <option value="2">Выбрать 2 штуки</option>
                        <option value="3">Выбрать 3 штуки</option>
                        <option value="4">Выбрать 4 штуки</option>
                        <option value="5">Выбрать 5 штук</option>
                    </select>
                    <hr >
                    <input type="submit" value="Заказать" class="btn btn-success">
                </form>
            </div>
            <?php endforeach;?>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="cartTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cartTitle"> В корзине <?php echo count($_SESSION["items"])?> товара</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul>
                        <?php foreach ($_SESSION['items'] as $item):?>
                        <li>Название: <?php echo $item['name']?> <br> Цена <?php echo $item['price']?> <br> Количество <?php echo $item['quantity']?></li>
                        <?php endforeach;?>
                    </ul>
                    <form method="post">
                        <input type="submit" name="clearCart" value="Очистить корзину">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary">Оформить заказ</button>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>